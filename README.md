# Flanderizer!

Created by [Kristin](https://gitlab.com/kristinbrooks) and [Chad](https://gitlab.com/thewoolleyman).

See it [live](https://upbeat-beaver-f4f7ac.netlify.app/)!

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
